/**
* PHP Email Form Validation - v3.2
* URL: https://bootstrapmade.com/php-email-form/
* Author: BootstrapMade.com
*/
(function () {
  "use strict";

  let forms = document.querySelectorAll('.php-email-form');

  forms.forEach( function(e) {
    e.addEventListener('submit', function(event) {
      event.preventDefault();

      let thisForm = this;

      let action = thisForm.getAttribute('action');
      let recaptcha = thisForm.getAttribute('data-recaptcha-site-key');
      
      if( ! action ) {
        displayError(thisForm, 'The form action property is not set!')
        return;
      }
      thisForm.querySelector('.loading').classList.add('d-block');
      thisForm.querySelector('.error-message').classList.remove('d-block');
      thisForm.querySelector('.sent-message').classList.remove('d-block');

      let formData = new FormData( thisForm );

      php_email_form_submit(thisForm, action, formData);
      // if ( recaptcha ) {
      //   if(typeof grecaptcha !== "undefined" ) {
      //     grecaptcha.ready(function() {
      //       try {
      //         grecaptcha.execute(recaptcha, {action: 'php_email_form_submit'})
      //         .then(token => {
      //           formData.set('recaptcha-response', token);
      //           php_email_form_submit(thisForm, action, formData);
      //         })
      //       } catch(error) {
      //         displayError(thisForm, error)
      //       }
      //     });
      //   } else {
      //     displayError(thisForm, 'The reCaptcha javascript API url is not loaded!')
      //   }
      // } else {
      //   php_email_form_submit(thisForm, action, formData);
      // }
    });
  });

  function php_email_form_submit(thisForm, action, formData) {
    
    //thisForm.querySelector('.edtext').innerHTML = "1 fetching";
    fetch(action, {
      method: 'POST',
      body: formData,
      headers: {'X-Requested-With': 'XMLHttpRequest'}
    })
    // .then(response => {
    //   thisForm.querySelector('.edtext').innerHTML = "2";
    //   if( response.ok ) {
    //     thisForm.querySelector('.edtext').innerHTML = "2 ok";
    //     return response.text()
    //   } else {
    //     thisForm.querySelector('.edtext').innerHTML = "2 else";
    //     throw new Error(`${response.status} ${response.statusText} ${response.url}`); 
    //   }
    // })
    .then(data => {
      //thisForm.querySelector('.edtext').innerHTML = "3" + data;
      thisForm.querySelector('.loading').classList.remove('d-block');
      //console.log("data", data);
      //if (data.trim() == 'OK') {
      if (data.ok) {
        //thisForm.querySelector('.edtext').innerHTML = "3 ok";
        thisForm.querySelector('.sent-message').classList.add('d-block');
        thisForm.reset(); 
      } else {
        //thisForm.querySelector('.edtext').innerHTML = "3 else";
        throw new Error(data ? data : 'Form submission failed and no error message returned from: ' + action); 
      }
    })
    .catch((error) => {
      //thisForm.querySelector('.edtext').innerHTML = "4 catch";
      displayError(thisForm, error);
    });
  }

  function displayError(thisForm, error) {
    thisForm.querySelector('.loading').classList.remove('d-block');
    thisForm.querySelector('.error-message').innerHTML = error;
    thisForm.querySelector('.error-message').classList.add('d-block');
  }

})();
